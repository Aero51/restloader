package rest.loader.activities;

import rest.loader.R;
import rest.loader.fragments.FlickrListFragment;
import rest.loader.fragments.InstaListFragment;
import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.widget.Toast;

public class MainActivity extends Activity {
	private static String acess_token ="639156055.e5d3b5e.81af669d771c4c3a8a09b54de1afb1bd";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		ActionBar actionbar = getActionBar();

		actionbar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

		Tab tab = actionbar.newTab();
		tab.setText("Instagram");
		TabListener<InstaListFragment> tl = new TabListener<InstaListFragment>(
				this, "Instagram", InstaListFragment.class);
		tab.setTabListener(tl);
		actionbar.addTab(tab);

		tab = actionbar.newTab();
		tab.setText("Flickr");
		TabListener<FlickrListFragment> t2 = new TabListener<FlickrListFragment>(
				this, "Flickr", FlickrListFragment.class);
		tab.setTabListener(t2);
		actionbar.addTab(tab);

		if (savedInstanceState != null) {
			actionbar.setSelectedNavigationItem(savedInstanceState.getInt(
					"tab", 0));
			
			
			
		
			
			
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		outState.putInt("tab", getActionBar().getSelectedNavigationIndex());
		super.onSaveInstanceState(outState);
	}

	private class TabListener<T extends Fragment> implements
			ActionBar.TabListener {

		private final Activity mActivity;
		private final String mTag;
		private final Class<T> mClass;
		private final Bundle mArgs;
		private Fragment mFragment;

		public TabListener(Activity activity, String tag, Class<T> clz) {
			this(activity, tag, clz, null);
		}

		public TabListener(Activity activity, String tag, Class<T> clz,
				Bundle args) {
			mActivity = activity;
			mTag = tag;
			mClass = clz;
			mArgs = args;

			mFragment = mActivity.getFragmentManager().findFragmentByTag(mTag);
			if (mFragment != null && !mFragment.isDetached()) {
				FragmentTransaction ft = mActivity.getFragmentManager()
						.beginTransaction();
				ft.detach(mFragment);
				ft.commit();
			}
		}

		@Override
		public void onTabSelected(Tab tab, FragmentTransaction ft) {
			if (mFragment == null) {
				mFragment = Fragment.instantiate(mActivity, mClass.getName(),
						mArgs);
				ft.add(R.id.fragment_container, mFragment, mTag);
			} else {
				ft.attach(mFragment);
			}
		}

		@Override
		public void onTabUnselected(Tab tab, FragmentTransaction ft) {
			if (mFragment != null) {
				// Detach the fragment, because another one is being attached
				ft.detach(mFragment);
			}
		}

		@Override
		public void onTabReselected(Tab tab, FragmentTransaction ft) {
			Toast.makeText(mActivity, "Reselected!", Toast.LENGTH_SHORT).show();
		}
	}
}
