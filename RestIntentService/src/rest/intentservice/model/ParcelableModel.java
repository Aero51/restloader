package rest.intentservice.model;

import android.os.Parcel;
import android.os.Parcelable;

public class ParcelableModel implements Parcelable {
    private String opis;
    private String slikaUrl;
    private String autor;

    public ParcelableModel(String opis, String slikaUrl, String autor )
    {
    	this.opis=opis;
    	this.slikaUrl=slikaUrl;
    	this.autor= autor;
    	
    }
    
    public ParcelableModel(Parcel source)
    {
    	this.opis=source.readString();
    	this.slikaUrl=source.readString();
    	this.autor= source.readString();	
    }
    
    public String getSlikaUrl() {
        return slikaUrl;
    }

    public String getAutor() {
        return autor;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public void setSlikaUrl(String slikaUrl) {
        this.slikaUrl = slikaUrl;
    }

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		  dest.writeString(this.opis);
	      dest.writeString(this.slikaUrl);
	      dest.writeString(this.autor);
		
	}

	  public static final Parcelable.Creator<ParcelableModel> CREATOR = new Parcelable.Creator<ParcelableModel>() {
		  
	        @Override
	        public ParcelableModel createFromParcel(Parcel source) {
	            return new ParcelableModel(source); // RECREATE MODEL GIVEN SOURCE
	        }
	 
	        @Override
	        public ParcelableModel[] newArray(int size) {
	            return new ParcelableModel[size]; // CREATING AN ARRAY OF MODEL
	        }
	    };
}