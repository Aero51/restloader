package rest.intentservice.fragments;

import rest.intentservice.R;
import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class FlickrListFragment extends Fragment {

	@Override
	public void onAttach(Activity activity) {
		Log.d("Cycles", "Flickr Fragment onAttach ");
		super.onAttach(activity);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		Log.d("Cycles", "Flickr Fragment onCreateView  ");
		return inflater.inflate(R.layout.flickrstubfragment, container, false);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		Log.d("Cycles", "Flickr Fragment onActivityCreated ");
		super.onActivityCreated(savedInstanceState);
		setRetainInstance(true);
	}

	@Override
	public void onStart() {
		Log.d("Cycles", "Flickr Fragment onStart ");
		super.onStart();
	}

	@Override
	public void onResume() {
		Log.d("Cycles", "Flickr Fragment onResume ");
		super.onResume();
	}

	@Override
	public void onPause() {
		Log.d("Cycles", "Flickr Fragment onPause ");
		super.onPause();
	}

	@Override
	public void onDestroyView() {
		Log.d("Cycles", "Flickr Fragment onDestroyView ");
		// TODO Auto-generated method stub
		super.onDestroyView();
	}

	@Override
	public void onDetach() {
		Log.d("Cycles", "Flickr Fragment onDetach ");
		// TODO Auto-generated method stub
		super.onDetach();
	}

}
