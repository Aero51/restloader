package rest.loader.rest;

public enum Scope {

    basic, comments, relationships, likes

}
