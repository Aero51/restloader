package rest.intentservice.rest;


import rest.intentservice.model.MediaResponse;
import rest.intentservice.model.Popular;
import rest.intentservice.model.SearchMediaResponse;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;

public class MediaEndpoint extends BaseEndpoint {

    private static interface MediaService {

        @GET("/media/{media_id}")
        public MediaResponse getMedia(
            @Path("media_id") String mediaId,
            @Query("access_token") String accessToken);

        @GET("/media/search")
        public SearchMediaResponse search(
            @Query("access_token") String accessToken,
            @Query("distance") Integer distanceInKm,
            @Query("lat") Double latitude,
            @Query("lng") Double longitude,
            @Query("min_timestamp") Long minTimestamp,
            @Query("max_timestamp") Long maxTimestamp);
       

        @GET("/media/popular")
        public Popular getPopular(
            @Query("client_id") String clientId);
        
       /* @GET("/media/popular")
        public void getPopular(
            @Query("client_id") String clientId, Callback<Popular> cb);*/
        
        
    //    @Query("acess_token") String accessToken);
    }

    private final MediaService mediaService;

    public MediaEndpoint(final String clientId, final RestAdapter.LogLevel logLevel) {
        super(clientId, logLevel);
        final RestAdapter restAdapter = new RestAdapter.Builder().setLogLevel(logLevel).setEndpoint(BASE_URL).build();
        mediaService = restAdapter.create(MediaService.class);
    }

    /*public void getPopular(Callback<Popular> cb) {
    	 mediaService.getPopular(clientId, cb);
    }*/

    public Popular getPopular() {
        return mediaService.getPopular(clientId);
    }

}
