package rest.loader.fragments;

import java.util.ArrayList;
import java.util.List;

import rest.loader.R;
import rest.loader.adapters.AdapterList;
import rest.loader.model.Media;
import rest.loader.model.Popular;
import rest.loader.rest.MediaEndpoint;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import android.app.ListFragment;
import android.app.LoaderManager;
import android.app.LoaderManager.LoaderCallbacks;
import android.content.Loader;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

public class InstaListFragment extends ListFragment {// implements
// Callback<Popular>{
	// LoaderCallbacks<List<Media>> {

	private static String client_id = "e5d3b5e00f70488fa244e02e9ec49e86";
	private static List<Media> list = new ArrayList<Media>();;
	private static AdapterList adapter;
	// private static LayoutInflater mInflater;
	private static Parcelable listViewState;
	private static MenuItem menuItem;
	// private Callback<Popular> cb;
	private MediaEndpoint eMediaEndpoint;

	private Callback<Popular> cb = new Callback<Popular>() {

		@Override
		public void success(Popular arg0, Response arg1) {

			Log.d("Callback", "huraaaa ");
			List<Media> lista = arg0.getMediaList();

			if (list.isEmpty())list = lista;

			adapter.setList(lista);
			adapter.notifyDataSetChanged();
			setListShown(true);

			if (list.hashCode() != lista.hashCode()) {
				list = lista;
				menuItem.collapseActionView();
				menuItem.setActionView(null);
			}

		}

		@Override
		public void failure(RetrofitError arg0) {
			Log.d("Callback", "Dogodio se retrofit error: " + arg0.getMessage());
		}
	};

	@Override
	public void onCreate(Bundle savedInstanceState) {
		Log.d("Cycles", "Insta Fragment onCreate ");
		setHasOptionsMenu(true);
		setRetainInstance(true);

		eMediaEndpoint = new MediaEndpoint(client_id, RestAdapter.LogLevel.NONE);
		eMediaEndpoint.getPopular(cb);

		super.onCreate(savedInstanceState);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		Log.d("Cycles", "Insta Fragment onActivityCreated ");

		if (adapter == null) {
			adapter = new AdapterList(getActivity(), list);
		} else {
			setListShown(true);
		}
		getListView().setAdapter(adapter);
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void onStart() {

		super.onStart();
	}

	@Override
	public void onResume() {
		if (listViewState != null) {

			getListView().onRestoreInstanceState(listViewState);
		}
		super.onResume();
	}

	@Override
	public void onPause() {
		super.onPause();
		listViewState = getListView().onSaveInstanceState();
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.menumain, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.refresh:
			Toast.makeText(getActivity(), "Refresh was clicked.",
					Toast.LENGTH_SHORT).show();
			menuItem = item;
			menuItem.setActionView(R.layout.progressbar);
			menuItem.expandActionView();
			eMediaEndpoint.getPopular(cb);

			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

}
