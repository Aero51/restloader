package rest.intentservice.services;



import rest.intentservice.model.Popular;
import rest.intentservice.rest.MediaEndpoint;
import retrofit.RestAdapter;

import de.greenrobot.event.EventBus;


import android.app.IntentService;
import android.content.Intent;

public class SimpleIntentService extends IntentService {
    public static final String PARAM_IN_MSG = "imsg";
    
    public SimpleIntentService() {
        super("SimpleIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {

    	String client_id = "e5d3b5e00f70488fa244e02e9ec49e86";
    	MediaEndpoint eMediaEndpoint= new MediaEndpoint(client_id, RestAdapter.LogLevel.NONE);	
    	Popular popular = eMediaEndpoint.getPopular();
    	EventBus.getDefault().post(popular);
        stopSelf();
    }
}
