package rest.intentservice.rest;


import rest.intentservice.model.Popular;
import retrofit.Callback;
import retrofit.RestAdapter;

public abstract class BaseEndpoint {

    protected static final String BASE_URL = "https://api.instagram.com/v1";

    protected final String clientId;
    protected final RestAdapter.LogLevel logLevel;
    protected Callback<Popular> cb;

    protected BaseEndpoint(final String clientId, final RestAdapter.LogLevel logLevel) {
        this.clientId = clientId;
        this.logLevel = logLevel;
        
    }

}
