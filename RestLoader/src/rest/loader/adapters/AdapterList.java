package rest.loader.adapters;

/**
 * Created by Nikola on 19.03.14..
 */
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;

import rest.loader.R;
import rest.loader.model.Media;
import rest.loader.rest.DrawableBackgroundDownloader;



public class AdapterList extends BaseAdapter {
	private final Context context;
	
	//private List<Model> aList = new ArrayList<Model>();
	private List<Media> aList = new ArrayList<Media>();
	public void setList(List<Media> list) {
		this.aList = list;
	}

	private DrawableBackgroundDownloader downloader = new DrawableBackgroundDownloader();

	public AdapterList(Context context, List<Media> list) {

		this.context = context;
		aList = list;

	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		ViewHolder holder;
		if (view == null) {
			view = LayoutInflater.from(context).inflate(R.layout.list_item,
					parent, false);
			
			holder = new ViewHolder();
			holder.picture = (ImageView) view.findViewById(R.id.slika);
			holder.author = (TextView) view.findViewById(R.id.autor);
			holder.description = (TextView) view.findViewById(R.id.opis);
			view.setTag(holder);

		} else {
			holder = (ViewHolder) view.getTag();
		}

		//Log.i("UndabotInsta", "pozicija:" + position);
		//holder.author.setText(aList.get(position).getAuthor());
		holder.author.setText(aList.get(position).getUser().getUsername());
		
		//holder.description.setText(aList.get(position).getDescription());
		
		String 	description=aList.get(position).getCaption().getText();
				holder.description.setText(description);
		
				
		Drawable drawable = context.getResources().getDrawable(
				R.drawable.no_image);
		//downloader.loadDrawable(aList.get(position).getPictureUrl(), holder.picture,drawable);
		downloader.loadDrawable(aList.get(position).getImages().getThumbnail().getUrl(), holder.picture,drawable);
		
		return view;
	}

	@Override
	public int getCount() {
		return aList.size();
	}

	@Override
	public String getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	static class ViewHolder {
		ImageView picture;
		TextView author;
		TextView description;
	}
}