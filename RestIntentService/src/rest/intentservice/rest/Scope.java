package rest.intentservice.rest;

public enum Scope {

    basic, comments, relationships, likes

}
