Example Android App for loading instagram popular photos
==================================================

 fetching of json data with  Retrofit, async photo download.
Used Action bar.

##Projects:

###RestLoader

-uses retrofit asynchronous fetching of json data with callback

###RestIntentService
-uses retrofit synchronous fetching of json data and EventBus posting from IntentService to main ui thread




##References: 

[EventBus presentation](http://www.slideshare.net/greenrobot/eventbus-for-android-15314813)

[Productive Android: EventBus](http://awalkingcity.com/blog/2013/02/26/productive-android-eventbus/)

[How to Use the EventBus Library](http://code.tutsplus.com/tutorials/quick-tip-how-to-use-the-eventbus-library--cms-22694)

[Durable Android REST Clients](http://www.mdswanson.com/blog/2014/04/07/durable-android-rest-clients.html)

[Retrofit by Square](http://kdubblabs.com/java/retrofit-by-square/)

[IntentService Basics](http://code.tutsplus.com/tutorials/android-fundamentals-intentservice-basics--mobile-6183)

[Passing Objects in Intents: Parcelables and More](http://http://thinkandroid.wordpress.com/2012/09/25/passing-objects-in-intents/)



##Todo:
 ~~-implement reusing of frags in main activity~~
 
~~-resolve problem with loader reset  upon change orientation of other(flickr) tab.(reset is implemented correctly- problem was in not understanding frag lifecycle - )~~

~~-prevent redownloading of pics on config change~~

-resolve json error that shows up occasionally (value null at caption of type org.json.JSON - empty caption produces error )

~~-implement saving of position in the list (Insta Frag - first tab)  upon orientation change when on second (flickr) tab~~

~~-implement refresh button in action bar(upper right corner) which downloads new set of popular photos~~



-implement appcompbat support

-research about memory leaks

##Screenshot:
![Screenshot](http://s29.postimg.org/5in5m87mf/2014_03_27_18_26_00.png)



