package rest.intentservice.adapters;

/**
 * Created by Nikola on 19.03.14..
 */
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;

import rest.intentservice.R;
import rest.intentservice.model.Media;
import rest.intentservice.model.ParcelableModel;
import rest.intentservice.rest.DrawableBackgroundDownloader;



public class AdapterList extends BaseAdapter {
	private final Context context;
//	private List<ParcelableModel> lista = new ArrayList<ParcelableModel>();
	private List<Media> aList = new ArrayList<Media>();
	
	
	public void setLista(List<Media> list) {
		this.aList = list;
	}
	
	/*public void setLista(List<ParcelableModel> lista) {
		this.lista = lista;
	}*/

	private DrawableBackgroundDownloader downloader = new DrawableBackgroundDownloader();

	public AdapterList(Context context, List<Media> list) {

		this.context = context;
		aList = list;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		ViewHolder holder;
		if (view == null) {
			view = LayoutInflater.from(context).inflate(R.layout.list_item,
					parent, false);
			holder = new ViewHolder();
			holder.picture = (ImageView) view.findViewById(R.id.slika);
			holder.author = (TextView) view.findViewById(R.id.autor);
			holder.description = (TextView) view.findViewById(R.id.opis);
			view.setTag(holder);

		} else {
			holder = (ViewHolder) view.getTag();
		}

		//Log.i("UndabotInsta", "pozicija:" + position);
	/*	holder.autor.setText(lista.get(position).getAutor());
		holder.opis.setText(lista.get(position).getOpis());

		Drawable drawable = context.getResources().getDrawable(
				R.drawable.no_image);
		downloader.loadDrawable(lista.get(position).getSlikaUrl(), holder.slika,
				drawable);
*/
		
	holder.author.setText(aList.get(position).getUser().getUsername());
		
		//holder.description.setText(aList.get(position).getDescription());
		
		String 	description=aList.get(position).getCaption().getText();
				holder.description.setText(description);
		
				
		Drawable drawable = context.getResources().getDrawable(
				R.drawable.no_image);
		//downloader.loadDrawable(aList.get(position).getPictureUrl(), holder.picture,drawable);
		downloader.loadDrawable(aList.get(position).getImages().getThumbnail().getUrl(), holder.picture,drawable);
		return view;
	}

	@Override
	public int getCount() {
		return aList.size();
	}

	@Override
	public String getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	static class ViewHolder {
		ImageView picture;
		TextView author;
		TextView description;
	}
}