package rest.intentservice.fragments;

import java.util.ArrayList;
import java.util.List;

import rest.intentservice.R;
import rest.intentservice.adapters.AdapterList;
import rest.intentservice.model.Media;
import rest.intentservice.model.Popular;
import rest.intentservice.services.SimpleIntentService;
import de.greenrobot.event.EventBus;

import android.app.ListFragment;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

public class InstaListFragment extends ListFragment {


	private static List<Media> list = new ArrayList<Media>();
	private static AdapterList adapter;
	private static Parcelable listViewState;
	private static MenuItem menuItem;

	private Intent msgIntent;

	public void onEventMainThread(Popular popular) {
		Log.d("onevent", "usao je u onevent........ ");
		List<Media> lista = popular.getMediaList();

		if (list.isEmpty())
			list = lista;

		adapter.setLista(lista);
		adapter.notifyDataSetChanged();
		setListShown(true);

		if (list.hashCode() != lista.hashCode()) {
			list = lista;
			menuItem.collapseActionView();
			menuItem.setActionView(null);
		}
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.d("Cycles", "Insta Fragment onCreate ");
		setHasOptionsMenu(true);
		setRetainInstance(true);

		EventBus.getDefault().register(this);
		msgIntent = new Intent(getActivity(), SimpleIntentService.class);
		getActivity().startService(msgIntent);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		Log.d("Cycles", "Insta Fragment onActivityCreated ");


		if (adapter == null) {
			adapter = new AdapterList(getActivity(), list);
		} else {
			setListShown(true);
		}
		getListView().setAdapter(adapter);

		
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void onStart() {

		super.onStart();
	}

	@Override
	public void onResume() {


		if (listViewState != null) {
			getListView().onRestoreInstanceState(listViewState);
		}
		super.onResume();
	}

	@Override
	public void onPause() {
		super.onPause();
		EventBus.getDefault().unregister(this);
		listViewState = getListView().onSaveInstanceState();
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.menumain, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.refresh:
			Toast.makeText(getActivity(), "Refresh was clicked.",
					Toast.LENGTH_SHORT).show();
			menuItem = item;
			menuItem.setActionView(R.layout.progressbar);
			menuItem.expandActionView();
			getActivity().startService(msgIntent);
			// setListShown(false);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

}


